# Typescript Express Starter 

Ready to start Typescript/Express Setup.

## System Requirements

- nodejs
- npm
- docker

## Configuration

Configuration can be done be .env-File.

## Installation

Run `docker-compose up` to start service containers.

## Usage & Examples

Visit `http://localhost:3000/health`.

## Testing

Run Jest by execute `npm run test`.

## Linting

Run ESLint by execute `npm run lint` or with autofix flag `npm run lint:fix`.

## License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
