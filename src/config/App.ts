import dotenv from 'dotenv';

/**
 * Class AppConfiguration
 */
class AppConfiguration {
  /**
   * Application Port
   */
  public port: string;

  /**
   * Constructor
   */
  constructor() {
    dotenv.config();

    this.port = process.env.APP_PORT || '3000';
  }
}

export = new AppConfiguration();
