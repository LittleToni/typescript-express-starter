import express, { Express, Request, Response } from 'express';
import AppConfiguration from './config/App';

/**
 * Class App
 */
class App {
  /**
   * Express app.
   * @private
   */
  private readonly _app: Express;

  /**
   * Get app instance.
   */
  get app() {
    return this._app;
  }

  /**
   * Constructor
   */
  constructor() {
    this._app = express();
    this._configure();
  }

  /**
   * Configure app instance
   * @private
   */
  private _configure() {
    this._app.use('/health', (req: Request, res: Response) => {
      res.json({
        status: 'success',
        statusCode: 200,
      });
    });
  }
}

// start app
new App()
  .app
  .listen(AppConfiguration.port);
